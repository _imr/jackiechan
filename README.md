jackiechan
==========

4chan.org HR image download written in python
## Usage:
```python
usage: jackiechan.py [-h] [-m] [-t] [-i] [-s] [-w destination] [-d dbase]
                     [-u main_url]

optional arguments:
  -h, --help            show this help message and exit
  -m, --mute            Just shut up and download. Virtually silent.
  -t, --threads         Do not show threads urls while parsing.
  -i, --img             Do not show vertical bar for each image downloaded.
  -s, --summary         Do not show summary after download completes.           [NOT IMPLEMENTED YET]
  -w destination, --dest destination      Override path to destination directory.
  -d dbase, --dbase dbase                 Override path to database file.
  -u main_url, --url main_url             Override main url. Usefull when you want to download
                                          something else than /hr.
```

## Example output:
```python
[P] Collecting thread URLs.
[P] Page: []
[P] Page: [2]
[P] Page: [3]
[P] Page: [4]
...
[A] Searching thread: 2213017   Found 170 links.
[A] Searching thread: 2230521   Found 146 links.
[A] Searching thread: 2233889   Found 165 links.
[A] Searching thread: 2241378   Found 227 links.
[A] Searching thread: 2242413   Found 189 links.
[A] Searching thread: 2247450   Found 95 links.
[A] Searching thread: 2248497   Found 129 links.
[A] Searching thread: 2251697   Found 21 links.
...
[A] Searching thread: 2252781   Found 38 links.
[A] Searching thread: 2255403   Found 5 links.
[A] Searching thread: 2258143   Found 18 links.
[A] Searching thread: 2261969   Found 26 links.
[A] Searching thread: 2261984   Found 23 links.
[A] Searching thread: 2266829   Found 36 links.
[A] Searching thread: 2268498   Found 22 links.
[i] DB size:  0
[i] New links:  1310
[↓] |||||||||||||||||||||||||||||||||||||||||||
[↓] |||||||||||||||||||||||||||||||||||||||||||
...
[i] DB Updated.
```

Explanation of prefixes used in output:

* `[P]` Preprocessing stage
* `[A]` Analysis in progress. Parsing webpages.
* `[i]` Informational output
* `[↓]` Downloading images
